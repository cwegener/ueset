Description
===========

`ueset` is currently in development. The code is just a proof of concept at
this stage.

Goals
=====

The overall design goals are:

- `ueset` does not require a database to store the configuration. The
  configuration is intended to be stored in a YAML text file which is supplied
  as a command line parameter.

- `ueset` does not require any kind of agent software on the target, it is
  intended to be a simple command line instead.

The core functionality consists of:

- Import registry values from a YAML definition file

- Copy files from source to destination as defined in a YAML definition file.

Installation
============

There are two ways to install `ueset`. It can be installed either as the python
source or as a standalone self-contained executable.

When installing as the python source, a Python 3.7 (or higher) interpreter must
already be available on the system(s) where `ueset` will run.

When installing as a self-contained executable (via `pyinstaller`), the
Python 3.7 interpreter is embedded into the self-contained executable.

Usage
=====

`ueset_registry -h`:

```
usage: ueset_registry [-h] file

Import HKCU registry values defined in a YAML file

positional arguments:
  file        path to settings yaml file

optional arguments:
  -h, --help  show this help message and exit
```
